// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Interactable.h"
#include "Kismet/KismetSystemLibrary.h"


// Sets default values
AFood::AFood()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	TeleportToNewLocation();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Intractor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Intractor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			TeleportToNewLocation();
		}
	}

}
void AFood::TeleportToNewLocation(int numOfTry)
{
	if (numOfTry >= 2500)
	{
		return;
	}
	float x, y;
	x = FMath::RandRange(-10, 10) * 50;
	y = FMath::RandRange(-10, 10) * 50;
	FVector Start = FVector(x, y, 0);
	FVector Finish = FVector(x, y, 20);

	FCollisionQueryParams RV_TraceParams = FCollisionQueryParams(FName(TEXT("RV_Trace")), true, this);
	RV_TraceParams.bTraceComplex = true;
	RV_TraceParams.bReturnPhysicalMaterial = false;

	
	FHitResult RV_Hit(ForceInit);

	//call to GetWorld() 
	bool bHit = GetWorld()->LineTraceSingleByChannel(
		RV_Hit,        
		Start,    
		Finish, 
		ECC_Pawn, 
		RV_TraceParams
	);
	if (bHit)
	{
		TeleportToNewLocation(numOfTry + 1);
		return;
	}

	SetActorLocation(FVector(x, y, 20));
}


