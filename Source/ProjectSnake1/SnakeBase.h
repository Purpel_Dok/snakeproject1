// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Chaos/ChaosEngineInterface.h"
#include "SnakeBase.generated.h"
class ASnakeElementBase;
class ASpawner;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class PROJECTSNAKE1_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Speed;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	UPROPERTY(BlueprintReadWrite)
		ASpawner* SpawnerActor;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY()
	FTimerHandle SpeedTimer;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

		UFUNCTION(BlueprintCallable)
		void AddSnakeElement(const int ElementsNum = 1);

		UFUNCTION()
		void Move();

		UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

		UFUNCTION()
		void KillSnake();

		UPROPERTY()
		float DefaulSpeed;

		UFUNCTION()
		void IncreaseSpeed(float SpeedMultiplier, float Time);

		UFUNCTION()
		void ReturnSpeed();

		UFUNCTION()
		void SpawnNewFood();
};
