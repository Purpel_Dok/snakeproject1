// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Spawner.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	LastMoveDirection = EMovementDirection::UP;
	MovementSpeed = 10.f;
	
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElement(const int ElementsNum)
{
	if (ElementsNum == 1)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		if (SnakeElements.Num() > 0)
		{
			NewLocation = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
		}
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();

		}
				
	}
	else
	{
		for (int i = 0; i < ElementsNum; i++)
			{
				FVector ShiftVector(SnakeElements.Num() * ElementSize, 0, 0);
				FTransform NewTransform = FTransform(GetActorLocation() - ShiftVector);
				NewTransform.SetScale3D(FVector(1, 1, 1));
				auto NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
				NewSnakeElem->SnakeOwner = this;
				int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

				if (ElemIndex == 0)
				{
					NewSnakeElem->SetFirstElementType();
				}
			}	
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	if (LastMoveDirection == EMovementDirection::UP)
	{
		MovementVector - FVector(ElementSize, 0, 0);
	}
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
		else
		{
			const FString CurrentLevelNameString = GetWorld()->GetName();
			const FName CurrentLevelName(CurrentLevelNameString);
			UGameplayStatics::OpenLevel(this, CurrentLevelName, false);
		}

	}
}

void ASnakeBase::KillSnake()
{
	for (auto elem : SnakeElements)
	{
		elem->Destroy();
	}
	Destroy();

}

void ASnakeBase::IncreaseSpeed(float SpeedMultiplier, float Time)
{
	if (SpeedTimer.IsValid())
		GetWorldTimerManager().ClearTimer(SpeedTimer);
	Speed /= SpeedMultiplier;
	SetActorTickInterval(Speed);
	GetWorldTimerManager().SetTimer(SpeedTimer, this, &ASnakeBase::ReturnSpeed, 1.0f, false, Time);
	 
	 
}

void ASnakeBase::ReturnSpeed()
{
	Speed = DefaulSpeed;
	SetActorTickInterval(Speed);
	GetWorldTimerManager().ClearTimer(SpeedTimer);
}

void ASnakeBase::SpawnNewFood()
{
	SpawnerActor->SpawnFood();
}

