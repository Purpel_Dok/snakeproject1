// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnBase.h"
#include "Food.h"
// Sets default values
ASpawnBase::ASpawnBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawnBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawnBase::SpawnFood()
{
	FVector NewLocation(FMath::FRandRange(-200, 200), FMath::FRandRange(-200, 200), 0);
	FTransform NewTransform = FTransform(NewLocation);
	GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
}

