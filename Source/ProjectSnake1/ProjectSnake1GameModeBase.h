// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectSnake1GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTSNAKE1_API AProjectSnake1GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
