// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h "
#include "Wall.generated.h"

UCLASS()
class PROJECTSNAKE1_API AWall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Interact(AActor* Actor);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
